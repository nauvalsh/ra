'use strict';

const faker = require('faker');
const _ = require('lodash');

function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function randomDate(start, end) {
  return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
}

let arr = [];
for (let i = 0; i < 30; i++) {
  arr.push({
    categoryId: getRandomInt(1, 4),
    title: faker.random.words(12),
    image: `dum-${getRandomInt(1, 4)}.jpg`,
    desc:
      'Lorem ipsum dolor sit amet, ea sed illud facilis percipit, errem euripidis vim ne. Esse feugiat perfecto nec ei, eum in voluptua omittantur, sint virtute ne eos. An inani inermis suscipiantur sea, vix appetere deseruisse no. Ei qui modus labore cotidieque, has dicunt labitur detraxit ea, eu mel inani inermis appellantur. Duo in consetetur dissentiunt, simul solet et his. <br/> <br/> Lorem ipsum dolor sit amet Id quo iudico persius accusata. Vix ut veri mundi tincidunt, vel cu menandri posidonium, altera labore vix ea. Vel no case mazim disputationi, vim id sale novum dissentiet, pro at purto splendide. Ea mel putant oblique posidonium, id nam justo dicunt appellantur. At velit blandit mel, sea clita argumentum an. <br/> <br/> His an putant iriure, ne duis affert percipitur has. His an graeci fierent consetetur, duo te quando veniam regione, menandri mnesarchum temporibus in qui. Te vero postea molestiae ius, enim fuisset expetendis has ea. Evertitur incorrupte ei vix. His ut legendos persecuti philosophia, et duo nostrud luptatum philosophia, sed mazim eligendi ut. <br/> <br/> Homero efficiendi eu his, ei mei ponderum intellegam efficiantur. Pro an rebum electram posidonium. Est ignota omnium quaeque cu. Everti suscipit qui ut. <br/> <br/> Lorem ipsum dolor sit amet Te admodum iudicabit vim, illud ubique numquam id cum. Nec in wisi accusata deseruisse, ius nusquam intellegam et. Habeo singulis id nam, an eos purto virtute adolescens. Usu te regione sanctus, sed et quem dolor voluptua. <br/> <br/> Lorem ipsum dolor sit amet, ea sed illud facilis percipit, errem euripidis vim ne. Esse feugiat perfecto nec ei, eum in voluptua omittantur, sint virtute ne eos. An inani inermis suscipiantur sea, vix appetere deseruisse no. Ei qui modus labore cotidieque, has dicunt labitur detraxit ea, eu mel inani inermis appellantur. Duo in consetetur dissentiunt, simul solet et his. <br/> <br/> Lorem ipsum dolor sit amet Id quo iudico persius accusata. Vix ut veri mundi tincidunt, vel cu menandri posidonium, altera labore vix ea. Vel no case mazim disputationi, vim id sale novum dissentiet, pro at purto splendide. Ea mel putant oblique posidonium, id nam justo dicunt appellantur. At velit blandit mel, sea clita argumentum an. <br/> <br/> His an putant iriure, ne duis affert percipitur has. His an graeci fierent consetetur, duo te quando veniam regione, menandri mnesarchum temporibus in qui. Te vero postea molestiae ius, enim fuisset expetendis has ea. Evertitur incorrupte ei vix. His ut legendos persecuti philosophia, et duo nostrud luptatum philosophia, sed mazim eligendi ut. <br/> <br/> Homero efficiendi eu his, ei mei ponderum intellegam efficiantur. Pro an rebum electram posidonium. Est ignota omnium quaeque cu. Everti suscipit qui ut. <br/> <br/> Lorem ipsum dolor sit amet Te admodum iudicabit vim, illud ubique numquam id cum. Nec in wisi accusata deseruisse, ius nusquam intellegam et. Habeo singulis id nam, an eos purto virtute adolescens. Usu te regione sanctus, sed et quem dolor voluptua. <br/> <br/>',
    isActive: true,
    createdAt: randomDate(new Date(2021, 0, 1), new Date()),
    updatedAt: new Date()
  });
}

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert('articles', arr, {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('products', null, {});
     */

    await queryInterface.bulkDelete('articles', null, {});
  }
};
