'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      'categories',
      [
        {
          categoryName: 'News'
        },
        {
          categoryName: 'Politik'
        },
        {
          categoryName: 'Teknologi'
        },
        {
          categoryName: 'Entertainment'
        }
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('categories', null, {});
     */

    await queryInterface.bulkDelete('categories', null, {});
  }
};
