const { Article, Category } = require('../models');
const { getAll, createOne, deleteOne, getOne } = require('./refactorController');

const getArticles = getAll(Article, 'articles', [{ model: Category, as: 'category' }]);
const getOneArticle = getOne(Article, 'article');
const createArticle = createOne(Article, 'article', 'image');
const deleteArticle = deleteOne(Article, 'article');

module.exports = {
  getArticles,
  createArticle,
  deleteArticle,
  getOneArticle
};
