const URL_LOCAL = `http://localhost:3000/`;
const API_LOCAL = `http://localhost:5000/testnode/`;
const SERVER_IMG_ARTICLES = `http://localhost:5000/testnode/images/articles/`;

export default {
  URL_LOCAL,
  API_LOCAL,
  SERVER_IMG_ARTICLES
};
