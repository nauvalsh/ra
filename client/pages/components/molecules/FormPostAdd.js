import dynamic from 'next/dynamic';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import vars from 'vars';

const ReactQuill = dynamic(() => import('react-quill'), { ssr: false });

const rq = {
  modules: {
    toolbar: [
      [{ 'header': '1' }, { 'header': '2' }, { 'font': [] }],
      [{ size: [] }],
      ['bold', 'italic', 'underline', 'strike', 'blockquote'],
      [
        { 'list': 'ordered' },
        { 'list': 'bullet' },
        { 'indent': '-1' },
        { 'indent': '+1' }
      ],
      ['link', 'image', 'video'],
      ['clean']
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false
    }
  },
  formats: [
    'header',
    'font',
    'size',
    'bold',
    'italic',
    'underline',
    'strike',
    'blockquote',
    'list',
    'bullet',
    'indent',
    'link',
    'image',
    'video'
  ]
};

const FormPostAdd = ({ token }) => {
  const [categories, setCategories] = useState([]);
  const [form, setForm] = useState({
    categoryId: null,
    title: '',
    image: '',
    desc: '',
    isActive: true
  });
  const [img, setImg] = useState({ source: '', preview: '' });

  useEffect(() => {
    async function getCategories() {
      const res = await fetch(`${vars.API_LOCAL}api/v1/categories`);
      const data = await res.json();

      setCategories(data.data.categories);
    }

    getCategories();
  }, []);

  const handleInputChange = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmitForm = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append('categoryId', form.categoryId);
    formData.append('title', form.title);
    formData.append('desc', form.desc);
    formData.append('image', img.source);
    formData.append('isActive', form.isActive);

    console.log(img.source);
    console.table([...formData]);
    console.info(token);

    // form data no need to specify multipart/form-data in headers
    const apiRes = await fetch(`${vars.API_LOCAL}api/v1/articles`, {
      method: 'POST',
      headers: {
        'Authorization': `Bearer ${token}`
      },
      body: formData
    });

    const data = await apiRes.json();

    if (apiRes.ok) {
      console.log(data);
      toast.info('Post berhasil ditambahkan', {
        position: toast.POSITION.TOP_CENTER
      });
      document.getElementById('formPost').reset();
      setForm({
        categoryId: null,
        title: '',
        image: '',
        desc: '',
        isActive: true
      });
      setImg({ source: '', preview: '' });
    } else {
      toast.error(data.message, {
        position: toast.POSITION.TOP_CENTER
      });

      return false;
    }
  };

  return (
    <form id="formPost" onSubmit={handleSubmitForm}>
      <div className="form my-4">
        <label>Title</label>
        <div className="form-control flex-1 flex my-1">
          <input
            type="text"
            className="border border-gray-300 p-2 text-sm w-full box-borderfocus:border-gray-200 focus:outline-none"
            placeholder="Title"
            onChange={handleInputChange}
            name="title"
          />
        </div>
      </div>
      <div className="form my-4">
        <label>Category</label>
        <div className="form-control flex-1 flex my-1">
          <select
            className="border border-gray-300 p-2 text-sm w-full box-borderfocus:border-gray-200 focus:outline-none"
            name="categoryId"
            id=""
            onChange={handleInputChange}
          >
            <option value="">-</option>
            {categories &&
              categories.map((cat, key) => (
                <option key={key} value={cat.id}>
                  {cat.categoryName}
                </option>
              ))}
          </select>
        </div>
      </div>
      <div className="form my-4">
        <label>Content</label>
        <div className="form-control my-1">
          <ReactQuill
            value={form.desc}
            onChange={(txt) => setForm({ ...form, desc: txt })}
            className="w-full h-auto"
            modules={rq.modules}
            formats={rq.formats}
          />
        </div>
      </div>
      <div className="form my-4">
        <label>Image</label>
        <div className="form-control flex-1 flex my-1">
          <input
            type="file"
            className="border border-gray-300 p-2 text-sm w-full box-borderfocus:border-gray-200 focus:outline-none"
            placeholder="Image"
            onChange={(e) => {
              const file = e.target.files[0];
              setImg({ ...img, source: file, preview: URL.createObjectURL(file) });
            }}
            name="image"
          />
        </div>
        {img.preview && <img src={img.preview} alt="" className="h-44" />}
      </div>
      <button className="border bg-primary py-2 px-5 text-white w-full my-4">
        Add Post
      </button>
    </form>
  );
};

export default FormPostAdd;
