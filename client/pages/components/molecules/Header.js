import Link from 'next/link';
import { useEffect, useState } from 'react';

const Header = () => {
  const [user, setUser] = useState(null);

  useEffect(() => {
    const usr = localStorage.getItem('user');

    setUser(JSON.parse(usr));
  }, []);

  const handleLogout = async (e) => {
    const res = await fetch(`/api/logout`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    });
    localStorage.removeItem('user');
    window.location.reload();
  };

  return (
    <header className="md:flex shadow-md z-[999] sticky top-0 font-sans-poppins justify-between items-center bg-white">
      <div className="py-4 px-14 flex flex-row">
        <a href="/" className="text-2xl text-primary">
          <strong>Designate.</strong>
        </a>

        <ul className="flex items-center ml-10">
          <li className=" justify-center mx-3">Event</li>
          <li className=" justify-center mx-3">Blog</li>
          <li className=" justify-center mx-3 text-xs">
            <select className="px-2" name="" id="">
              <option value="">ID</option>
            </select>
          </li>
        </ul>
      </div>
      <div className="py-4 px-14 flex flex-row">
        <ul className="ml-4 flex items-center">
          <li className=" justify-center mx-3">
            <Link href={user ? '/cms' : '/login'}>
              <a>{user ? `Hi, ${user.name}` : 'Masuk'}</a>
            </Link>
          </li>
          <li className=" justify-center mx-3">
            {user && (
              <p className="cursor-pointer" onClick={handleLogout}>
                <a>Keluar</a>
              </p>
            )}
            {!user && (
              <p className="cursor-pointer">
                <Link href="/register">
                  <a>{user ? `Keluar` : 'Daftar'}</a>
                </Link>
              </p>
            )}
          </li>
          <li className=" justify-center mx-3 text-primary px-4 py-2 border-2 border-primary">
            Perusahaan
          </li>
        </ul>
      </div>
    </header>
  );
};

export default Header;
