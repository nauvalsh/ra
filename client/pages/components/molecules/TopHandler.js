import FilterContext from 'context/FilterContext';
import { useContext } from 'react';

const TopHandler = () => {
  const { filter, setFilter } = useContext(FilterContext);

  return (
    <div className="my-20 m-auto bg-white font-sans-poppins flex justify-between flex-col md:flex-row">
      <div className="form-control flex-1 w-3/4 flex pr-4">
        <input
          type="text"
          className="border border-gray-300 p-2 text-sm w-full box-border border-r-0 focus:border-gray-200 focus:outline-none"
          placeholder="Masukan kata kunci atau judul artikel"
          onChange={(e) => setFilter(e.target.value)}
        />
        <button className="border bg-primary p-2 relative right-2 text-white px-4">
          Cari
        </button>
      </div>
      <div className="form-control md:w-1/4 flex flex-row text-xs text-gray-500  md:justify-end ">
        <select className="px-4 border border-gray-300 mx-2" name="" id="">
          <option value="">Kategori</option>
        </select>
        <select className="px-4 border border-gray-300 mx-2" name="" id="">
          <option value="">Urutkan</option>
        </select>
      </div>
    </div>
  );
};

export default TopHandler;
