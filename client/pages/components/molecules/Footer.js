const Footer = () => {
  return (
    <div className="bg-[#EE7000] px-14 text-white py-6 font-sans-poppins">
      <div className=" md:flex justify-between xl:max-w-6xl mx-auto">
        <div className="left-footer w-1/3">
          <label className="text-white">
            <h1 className="text-4xl font-bold">Designate.</h1>
          </label>
          <p className="text-xs">Solve your problems with our services</p>
          <p className="mt-4 text-sm">
            District 8, SCBD Lot 28 <br />
            Jl. Jenderal Sudirman Kav. 52-53 <br />
            DKI Jakarta 12190 <br />
            Indonesia <br />
            Phone : 021-5431 334
          </p>
        </div>
        <div className="right-footer flex w-1/2 justify-between">
          <div>
            <label className="text-white">
              <h1 className="text-2xl font-bold">Feature</h1>
            </label>
            <p className="mt-4 text-sm">
              <ul>
                <li>Overview</li>
                <li>Design</li>
                <li>Programming</li>
                <li>Collaborate</li>
              </ul>
            </p>
          </div>
          <div>
            <label className="text-white">
              <h1 className="text-2xl font-bold">Know us more</h1>
            </label>
            <p className="mt-4 text-sm">
              <ul>
                <li>About us</li>
                <li>Testimonial</li>
                <li>FAQ</li>
                <li>Terms & Conditions</li>
              </ul>
            </p>
          </div>
          <div>
            <label className="text-white">
              <h1 className="text-2xl font-bold">About us</h1>
            </label>
            <p className="mt-4 text-sm">
              <ul>
                <li>Stories</li>
                <li>Communities</li>
                <li>Career</li>
              </ul>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
