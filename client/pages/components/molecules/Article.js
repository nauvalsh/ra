import dayjs from 'dayjs';
import Link from 'next/link';

const Article = ({ img, title, linkTo, category, date }) => {
  return (
    <div className="w-[49%] mb-6 md:flex flex-row border border-gray-300 rounded-md md:max-h-[240px] overflow-hidden text-gray-800">
      <div className="relative overflow-hidden md:w-0 md:pr-[50%] pb-[49%] h-0 rounded-l-md">
        <img
          className="m-auto absolute top-0 left-0 bottom-0 h-full w-full object-cover transform hover:scale-125 transition-transform ease-in-out"
          src={img}
          style={{ objectFit: 'cover' }}
        />
      </div>
      <div className="mx-2"></div>
      <div className="md:w-1/2 py-2 pr-2">
        <p className="flex justify-end pr-2">
          <svg
            className="w-5 h-5 mr-4"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M8.684 13.342C8.886 12.938 9 12.482 9 12c0-.482-.114-.938-.316-1.342m0 2.684a3 3 0 110-2.684m0 2.684l6.632 3.316m-6.632-6l6.632-3.316m0 0a3 3 0 105.367-2.684 3 3 0 00-5.367 2.684zm0 9.316a3 3 0 105.368 2.684 3 3 0 00-5.368-2.684z"
            ></path>
          </svg>

          <svg
            className="w-5 h-5"
            fill="none"
            stroke="currentColor"
            viewBox="0 0 24 24"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
            ></path>
          </svg>
        </p>
        <Link href={linkTo || ''}>
          <a>
            <h1 className="font-semibold mt-4 text-gray-700">{title}</h1>
          </a>
        </Link>
        <div className="h-4"></div>
        <p className="text-secondary text-xs font-semibold">{category}</p>
        <div className="h-4"></div>
        <p className="text-xs font-semibold text-gray-400">
          {dayjs(date).format('DD MMMM YYYY')}
        </p>
      </div>
    </div>
  );
};

export default Article;
