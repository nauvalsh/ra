import cookie from 'cookie';
import 'react-quill/dist/quill.snow.css';
import { parseCookies } from 'utils/helpers';
import vars from 'vars';
import { Layout } from './components/molecules';
import FormCategory from './components/molecules/FormCategory';
import FormPostAdd from './components/molecules/FormPostAdd';

export async function getServerSideProps({ req, res }) {
  const { token } = parseCookies(req);

  if (!req.headers.cookie) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    };
  }

  const cookies = cookie.parse(req.headers.cookie);

  // some auth logic here
  const apiRes = await fetch(`${vars.API_LOCAL}api/v1/users/me`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${cookies.token}`
    }
  });

  const data = await apiRes.json();

  if (!data?.user?.id) {
    return {
      redirect: {
        destination: '/login',
        permanent: false
      }
    };
  }

  return {
    props: { token } // will be passed to the page component as props
  };
}

const Cms = ({ token }) => {
  return (
    <>
      <Layout title="Designate — Platform media kolaboratif">
        <div className="max-w-6xl mx-auto p-2 z-0 my-10">
          <FormCategory token={token} />

          <hr className="my-10" />

          <FormPostAdd token={token} />
        </div>
      </Layout>
    </>
  );
};

export default Cms;
