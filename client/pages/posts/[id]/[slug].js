import { useRouter } from 'next/router';
import { Layout } from 'pages/components/molecules';
import Article from 'pages/components/molecules/Article';
import CommentPost from 'pages/components/molecules/CommentPost';
import { useState } from 'react';
import ReactHtmlParser from 'react-html-parser';
import vars from '../../../vars';

const ProductSlug = ({ articleData }) => {
  const router = useRouter();
  const [image, setImage] = useState('https://dummyimage.com/500x500/400/fff');
  const [article, setArticle] = useState(articleData || null);

  const handleClickImage = (e) => {
    const img = e.target.getAttribute('src');
    setImage(img);
  };

  return (
    <Layout title={`${article.title} — Designate`}>
      <div className="max-w-4xl mx-auto p-2 z-0 font-sans-cabin">
        <h1 className="text-4xl font-semibold mt-6">{article && article.title}</h1>
        <p>
          <ul className="flex text-sm my-5">
            <li className="text-secondary">Tips dan Trik</li>
            <div className="mx-4"></div>
            <li className="text-gray-600">22 September 2020</li>
          </ul>
        </p>
        <p>
          <img
            className="w-full"
            src={`${vars.SERVER_IMG_ARTICLES}${article.image}`}
            alt=""
          />
        </p>
        <p className="my-2 text-gray-700">{article && ReactHtmlParser(article.desc)}</p>

        <div className="flex justify-between">
          <p className="flex items-center">
            <img src="/clap.svg" /> <div className="px-2"></div>
            200 Claps
          </p>
          <p className="flex items-center">
            Share on <div className="px-2"></div>
            <img src="/sos-fb.svg" />
            <div className="px-2"></div> <img src="/sos-twit.svg" />{' '}
            <div className="px-2"></div>
            <img src="/sos-link.svg" />
            <div className="px-2"></div> <img src="/sos-copy.svg" />{' '}
            <div className="px-2"></div>
          </p>
          {/* Sosmed */}
          <p className="flex items-center">
            Bookmark <div className="px-2"></div>
            <svg
              className="w-6 h-6 text-primary"
              fill="none"
              stroke="currentColor"
              viewBox="0 0 24 24"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M5 5a2 2 0 012-2h10a2 2 0 012 2v16l-7-3.5L5 21V5z"
              ></path>
            </svg>
          </p>
        </div>

        <hr className="my-6" />

        {/* Author */}
        <div className="flex text-gray-800">
          <div className="">
            <div className="w-[80px]">
              <img src="/author.png" />
            </div>
          </div>
          <div className="mr-6"></div>
          <div className="">
            <p className="text-sm text-gray-500">About Author</p>
            <p className="text-lg font-semibold mb-2">James Stewart</p>
            <p className="text-sm">
              James Stewart works at TotalKarir, helping event creators level-up their
              game and connect with one another. Born and raised in New Orleans, there’s
              nothing he enjoys more than helping people get together.
            </p>
          </div>
        </div>

        <hr className="my-6" />
        {/* Comment header */}
        <div className="flex justify-between">
          <div>6 Comments</div>
          <div>
            <ul className="flex">
              <li className="mr-4">Newest</li>
              <li className="mr-4 text-gray-500">Oldest</li>
              <li className="text-gray-500">Liked</li>
            </ul>
          </div>
        </div>
        {/* Form comment Section */}
        <div className="form-control flex-1 flex my-6">
          <input
            type="text"
            className="border border-gray-300 p-2 text-sm w-full box-borderfocus:border-gray-200 focus:outline-none"
            placeholder="Add comment here..."
          />
          <button className="border bg-primary py-2 px-5 text-white ml-4">Add</button>
        </div>

        {/* Comment post */}
        <CommentPost />
        <CommentPost />
        <CommentPost />
      </div>
      {/* Related Post */}
      <div className="max-w-5xl px-4 mx-auto">
        <h1 className="text-gray-800  font-semibold text-xl mt-10 mb-6">Related Post</h1>
        <div className="flex flex-wrap justify-between">
          <Article img="./../../dummy-article.png" />
          <Article img="./../../dummy-article.png" />
          <Article img="./../../dummy-article.png" />
          <Article img="./../../dummy-article.png" />
        </div>
      </div>
    </Layout>
  );
};

export default ProductSlug;

export async function getServerSideProps(ctx) {
  const { id, slug } = ctx.query;
  // Fetch data from external API
  const res = await fetch(`${vars.API_LOCAL}api/v1/articles/${id}`);
  const data = await res.json();

  const articleData = data?.article ? data.article : null;

  // Pass data to the page via props
  return { props: { articleData: articleData } };
}
