import cookie from 'cookie';
import vars from 'vars';

export default async (req, res) => {
  if (req.method === 'POST') {
    const { email, password } = req.body;

    const apiRes = await fetch(`${vars.API_LOCAL}api/v1/auth/login`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email,
        password
      })
    });

    const data = await apiRes.json();

    if (apiRes.ok) {
      // Set Cookie
      res.setHeader(
        'Set-Cookie',
        cookie.serialize('token', data.data.token.accessToken, {
          httpOnly: true,
          secure: process.env.NODE_ENV !== 'development',
          maxAge: 60 * 60 * 24 * 7, // 1 week
          sameSite: 'strict',
          path: '/'
        })
      );

      return res.status(200).json({ user: data.data.user });
    } else {
      return res.status(data.error.statusCode).json({ message: data.message });
    }
  } else {
    res.setHeader('Allow', ['POST']);
    res.status(405).json({ message: `Method ${req.method} not allowed` });
  }
};
