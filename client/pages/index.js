import FilterContext from 'context/FilterContext';
import { useContext, useEffect, useState } from 'react';
import vars from '../vars';
import { Layout } from './components/molecules';
import Article from './components/molecules/Article';
import TopHandler from './components/molecules/TopHandler';

export default function Home({ articlesData }) {
  const { filter, setFilter } = useContext(FilterContext);

  const [articles, setArticles] = useState(
    articlesData?.data?.articles ? articlesData?.data?.articles : []
  );
  const [page, setPage] = useState(1);

  // const [filterArticle, setFilterArticle] = useState([]);

  // const handleSearch = (e) => {
  //   setFilterArticle(e.target.value);
  // };

  useEffect(() => {
    setTimeout(() => {
      if (filter.length > 0) {
        let filteredAticles = articles.filter((el) =>
          el.title.toLowerCase().includes(filter.toLowerCase())
        );

        console.log(filteredAticles);

        setArticles(filteredAticles);
      } else {
        setArticles(articlesData?.data?.articles);
      }
    }, 1500);
  }, [filter]);

  const handleLoadMore = async (e) => {
    const res = await fetch(`${vars.API_LOCAL}api/v1/articles?page=${page + 1}&limit=10`);
    const data = await res.json();
    const newarticles = data.data.articles;
    setPage(page + 1);

    setArticles([...articles, ...newarticles]);
  };

  return (
    <>
      <Layout title="Designate — Platform media kolaboratif">
        <div className="max-w-6xl mx-auto p-2 z-0">
          <TopHandler />
          <div className="flex flex-wrap justify-between">
            {articles &&
              articles.length > 0 &&
              articles.map((article, index) => (
                <Article
                  img={`${vars.SERVER_IMG_ARTICLES}${article.image}`}
                  title={article.title}
                  linkTo={`/posts/${article.id}/${article.slug}`}
                  key={index}
                  category={article.category.categoryName}
                  date={article.createdAt}
                />
              ))}
          </div>

          {filter.length < 1 && (
            <div className="flex justify-center py-4">
              <p
                className=" cursor-pointer text-center bg-primary text-white text-lg inline-block px-4 py-2"
                onClick={handleLoadMore}
              >
                Load More
              </p>
            </div>
          )}
        </div>
      </Layout>
    </>
  );
}

export async function getServerSideProps() {
  // Fetch data from external API
  const res = await fetch(`${vars.API_LOCAL}api/v1/articles?page=1&limit=10`);
  const data = await res.json();

  // Pass data to the page via props
  return { props: { articlesData: data } };
}
