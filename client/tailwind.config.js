module.exports = {
  mode: 'jit',
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      fontFamily: {
        'sans-poppins': ['Poppins'],
        'sans-cabin': ['Cabin']
      },
      colors: {
        'primary': '#FE7902',
        'secondary': '#0072B1'
      }
    }
  },
  variants: {
    extend: { scale: ['active', 'group-hover'] }
  },
  plugins: []
};
